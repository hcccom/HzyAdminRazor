# HZY.AdminRazor



### :yellow_heart: 介绍 
后台通配权限管理系统！基于.Net5、FreeSql

### :blue_heart: 代码生成器
https://gitee.com/hzy6/hzy-admin-code-generation

### 后台模板
https://gitee.com/hzy6/hzy-admin-ui

###  :cherries: 前后端分离版
https://gitee.com/hzy6/HzyAdminSpa

#### 软件架构
开发环境：vs2019 、 .Net5.0

###  **_交流群: 534584927_** 


前端：BootStrap 4、Layer(只是弹层 没用到 layui)、Jquery、Vue 、Element UI

数据库脚本位置：根目录/doc/HZY.Admin.sql


#### 部分界面截图
![登录](https://images.gitee.com/uploads/images/2020/0314/224558_f569321d_1242080.png "屏幕截图.png")
![主页](https://images.gitee.com/uploads/images/2020/0811/200450_a13eb19f_1242080.png "屏幕截图.png")
![用户管理](https://images.gitee.com/uploads/images/2020/0811/200618_0fc77246_1242080.png "屏幕截图.png")
![组件展示页](https://images.gitee.com/uploads/images/2020/0402/230647_c5ad87dc_1242080.png "屏幕截图.png")
![会员管理](https://images.gitee.com/uploads/images/2020/0401/104952_b7ce1127_1242080.png "屏幕截图.png")
![操作日志](https://images.gitee.com/uploads/images/2020/0811/200826_91d63164_1242080.png "屏幕截图.png")
![换肤](https://images.gitee.com/uploads/images/2020/0401/105141_f38425ce_1242080.png "屏幕截图.png")
![角色功能](https://images.gitee.com/uploads/images/2020/0401/105211_b390503f_1242080.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1201/183418_626fb029_1242080.png "屏幕截图.png")


#### 使用说明

如果部署iis访问不了的情况可以用两种办法：

1、直接打开exe然后控制台看错误

2、web.config里面有个false 改为 true，iis重启项目后运行网站后，跟目录下面 有个文件夹 log 里面有错误日志文件

3、一定注意 项目 bin/netcoreapp2.2/debug/app.xml 放入部署项目的跟目录中 


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

####  :heart: 捐赠 兄弟们就当打发要饭的吧！
![输入图片说明](https://images.gitee.com/uploads/images/2020/1216/105734_96c2122c_1242080.png "未标题-1.png")